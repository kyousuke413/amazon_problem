<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class StockerController extends AppController
{
    /**
     * 最初に呼び出される関数functionの内容にそって、以下に定義される関数を呼び出す
     */
    public function index()
    {
        $this->autoRender = false;
        $query = $query = $this->request->getQueryParams();
       
        if(!isset($query['function']))$this->error();
        
        switch ($query['function'])
        {
            case  "addstock": 
                $this->addstock($query);
                break;
            case  "checkstock": 
                $this->checkstock($query);
                break;
            case  "sell": 
                $this->sell($query);
                break;
            case  "checksales": 
                $this->checksales($query);
                break;
            case  "deleteall": 
                $this->deleteall($query);
                break;
            default :
                $this->error();
        }
    }
    
    /*　商品を追加する関数
     *　同じ名前の商品があった時は、与えられたamountをその商品のamountに追加する
     *  amount省略時およびamountの値がnullの際には1に変更 
     *  nameがないときはerrorを出力
     *  amountの値が整数ではない時error
     */
    public function addstock($query)
    {
        if(!isset($query['name']))$this->error();
        
        $stockers = TableRegistry::get('Stockers');
        $stocker=$stockers->find('all')->where(['name'=>$query['name']])->first();
       
        if(!isset($query['amount']))
        {
            $amount = 1;
        }else if(!ctype_digit($query['amount']))
        {
            $this->error();
        }else{
            $amount = $query['amount'] = null ? 1: $query['amount'];
        }

        if(!$stocker)
        {
            $stocker = $stockers->newEntity();
            $stocker->name = $query['name'];
            $stocker->amount = $amount;
        }else{
            $stocker->name = $query['name'];
            $stocker->amount += $amount;
        }
        $stockers->save($stocker);
        
    }
    
    /*  在庫チェックの関数
     *　nameの指定がない時はnameに関して昇順で在庫を出力
     *  与えらえたnameに対応するデータがない時はerrorを出力
     */
    public function checkstock($query)
    {
        $stockers = TableRegistry::get('Stockers');
        if(isset($query['name']))
        {   
            $stocker = $stockers->find('all')->where(['name'=>$query['name']])->first();
            if(!$stocker)
            {
                $this->error();
            }else{
                echo sprintf('%s: %s',$stocker->name,$stocker->amount);
            }
        }else
        {
            $stockers = $stockers->find()->order(['name' => 'ASC'])->toArray();;
            for($i=0;$i<count($stockers);$i++)
            {
                if($stockers[$i]->amount!=0)
                {
                    echo sprintf("%s: %s\n",$stockers[$i]->name,$stockers[$i]->amount);
                }
            }
        }
    }
    
    /*  販売関数
     *　nameの指定がない時はerrorを出力
     *  amount省略時およびamountの値がnullの際には1に変更
     *  与えらえたnameに対応するデータがない時はerrorを出力
     *  与えらえたamountの値が商品のamountを超えた際にはerrorを出力
     */
    public function sell($query)
    {
        if(!isset($query['name'])) $this->error();
        
        if(!isset($query['amount']))
        {
            $amount = 1;
        }else{
            $amount = $query['amount'] = null ? 1 : $query['amount'];
        }
        //debug($amount);
        $stockers = TableRegistry::get('Stockers');
        $stocker = $stockers->find('all')->where(['name'=>$query['name']])->first();
        if($amount <= $stocker->amount)//クエリのアマウントの最大値はsqlのアマウント以下
        {
            
            $stocker->sales = isset($query['price']) ? ($stocker->sales + $amount*$query['price']) : 0;
            $stocker->amount = $stocker->amount - $amount;
            $stockers->save($stocker);
        }else
        {
            echo "ERROR";
        }
        
    }
    /*  売り上げチェックする関数
     *　売り上げた値は小数点第二位まで表示されるよう切り上げ
     */
    public function checksales($query)
    {
        $stockers = TableRegistry::get('Stockers');
        $stockers = $stockers->find()->toArray();
        $val=0;
        for($i=0;$i<count($stockers);$i++)
        {
            $val = $val + $stockers[$i]->sales;
        }
        $val = $val*10*10;
        $val=ceil($val);
        $val=$val/(10*10);
        echo sprintf('sales: %s',$val);
    }
    /**
     * 全データを削除する関数
     */
    public function deleteall($query)
    {
        $stockers = TableRegistry::get('Stockers');
        $stocker = $stockers->find()->toArray();
        for($i=0;$i<count($stocker);$i++)
        {
            $stockers->delete($stocker[$i]);
        }
    }
    
   /**
    * ERRORを表示してプログラムを停止する関数
    */
    public function error(){
        echo "ERROR";
        die;
    }
}
