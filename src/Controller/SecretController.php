<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class SecretController extends AppController
{
   public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->autoRender = false;
        $loginId = 'amazon';
        $password = 'candidate';

        if (isset($_SERVER['PHP_AUTH_USER'])) {
            if (! ($_SERVER['PHP_AUTH_USER'] === $loginId && $_SERVER['PHP_AUTH_PW'] === $password)) {
                $this->_basicUnauthorized();
            }
        } else {
            $this->_basicUnauthorized();
        }
        $this->autoRender = true;
    }

    protected function _basicUnauthorized()
    {
        header('WWW-Authenticate: Basic realm="Please enter your ID and password"');
        header('HTTP/1.0 401 Unauthorized');
        die("Authorization Required");
    }
    
    public function index(){
        $this->viewBuilder()->layout('');
    }
}
