<?php
namespace App\Controller;

use App\Controller\AppController;
use \Exception;
use jlawrence\eos\Parser;

class CalcController extends AppController
{
   
    public function index()
    {
       set_error_handler(
           function ($errno, $errstr, $errfile, $errline) {
               // エラーが発生した場合、ErrorExceptionを発生させる
               throw new Exception();
               
           });
           
        $this->autoRender = false;
        $query = $_SERVER['QUERY_STRING'];
        $pattern="/[^\+\-\*\/()0-9]/";
        if(!preg_match($pattern,$query)) 
        {
            //$result=eval("return ".$query.";");
            try
            {
                $result = Parser::solve($query);
            }catch(Exception $e){
                $result = "ERROR";            
            }
            
            echo $result;
        }else{
            echo"ERROR";
        }
    }

}