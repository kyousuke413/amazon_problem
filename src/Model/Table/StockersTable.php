<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Stockers Model
 *
 * @method \App\Model\Entity\Stocker get($primaryKey, $options = [])
 * @method \App\Model\Entity\Stocker newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Stocker[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Stocker|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Stocker patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Stocker[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Stocker findOrCreate($search, callable $callback = null, $options = [])
 */
class StockersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('stockers');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('amount', 'create')
            ->notEmpty('amount');

        $validator
            ->numeric('price')
            ->allowEmpty('price');

        $validator
            ->numeric('sales')
            ->allowEmpty('sales');

        return $validator;
    }
}
