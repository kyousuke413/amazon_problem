<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StockersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StockersTable Test Case
 */
class StockersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StockersTable
     */
    public $Stockers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.stockers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Stockers') ? [] : ['className' => StockersTable::class];
        $this->Stockers = TableRegistry::get('Stockers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Stockers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
